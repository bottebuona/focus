<?php
/**
 * Template part for displaying pages on front page
 *
 */

?>

<article id="panelvideo" "twentyseventeen-panel twentyseventeenchild-panel">

	<div class="panel-content video-panel-content">
		<div class="wrap">
			<div class="video-panel-content-text">
					<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>

					<?php twentyseventeen_edit_link( get_the_ID() ); ?>

				<div class="entry-content">
					<?php
						/* translators: %s: Name of current post */
						the_content( sprintf(
							__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeenchild' ),
							get_the_title()
							) );
					?>
					<ul class="os-list">
						<li>
						<a href="#panelvideo" class="os-list-link">
							<svg class="icon icon-android" aria-hidden="true" role="img">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-android"></use>
							</svg>
						</a>
						</li>
						<li>
							<a href="#panelvideo" class="os-list-link">
							<svg class="icon icon-apple" aria-hidden="true" role="img">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-apple"></use>
							</svg>
						</a>
						</li>
						<li>
							<a href="#panelvideo" class="os-list-link">
							<svg class="icon icon-windows" aria-hidden="true" role="img">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-windows"></use>
							</svg>
						</a>
						</li>
					</ul>
				</div><!-- .entry-content -->
			</div><!-- .video-panel-content-text -->

			<div class="video-panel-content-video">
				<?php
				echo wp_oembed_get( get_field( 'video_url' ) );
				?>
			</div><!-- .video-panel-content-video -->

		</div><!-- .wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
