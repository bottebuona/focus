<?php
/**
 * Displays content for front page
 *
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel ' ); ?> >

	<div class="panel-content">
		<div class="wrap features-wrap">

			<div class="entry-content">

					<div class="row">
						<div class="features-leftcol">
							<?php echo focus_feature_card('1'); ?>
							<?php echo focus_feature_card('2'); ?>
						</div>
						<div class="features-centercol">
							<div class="phone-mockup"><img src="<?php echo get_theme_file_uri() ?>/assets/images/phone.png"></div>
						</div>
						<div class="features-rightcol">
							<?php echo focus_feature_card('3'); ?>
							<?php echo focus_feature_card('4'); ?>
						</div>
					</div><!-- .row -->

			</div><!-- .entry-content -->

		</div><!-- .wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
