<?php
/**
* Template part for displaying section on front page
*
*/

global $twentyseventeencounter;

?>

<article id="panelcontact" class="twentyseventeen-panel twentyseventeenchild-panel" >

	<div class="panel-content">
		<div class="wrap">
			<header class="panelcontact-header">
				<h2 class="panelcontact-title">Say Hi &amp; Get in Touch</h2>
			</header><!-- .entry-header -->
			<p class="panelcontact-text">Expendere obdormiam im cunctatus opportune re veritates to infirmari.</p>
			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<div class="panelcontact-links social-navigation">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'social',
						'menu_class'     => 'social-links-menu',
						'depth'          => 1,
						'link_before'    => '<span class="screen-reader-text">',
						'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
						) );
						?>
					</div>
				<?php endif; ?>
			</div><!-- .wrap -->
		</div><!-- .panel-content -->

	</article>
