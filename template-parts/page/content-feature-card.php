<?php
/**
 * Template part for displaying feature card on front page
 * features section
 */
global $childfeaturenum;
 ?>

<div class="feature-card">
  <h2 class="feature-headline"> <?php the_field( 'feature_' . $childfeaturenum . '_headline' ); ?></h2>
  <p class="feature-text"> <?php the_field('feature_' . $childfeaturenum . '_text'); ?></p>
</div>
