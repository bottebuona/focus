<?php
/**
* Template part for displaying testimonials list on front page
*
*/

?>
<article id="paneltestimonials" class="twentyseventeen-panel twentyseventeenchild-panel">

	<div class="panel-content panel-testimonials-content">
		<div class="wrap">

			<?php
			// WP query args
			$args = array(
	      'post_type'   => 'child-testimonials',
	    //  'nopaging'    => true
	    );
			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) : ?>
				<!-- the loop -->
				<div class="testimonials-wrap">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="testimonials-item">

					<div class="testimonials-card">
						<?php
						$content = get_the_content();
						$content = substr($content, 0,90); // limit text lenght to one row
						?>
						<div class="testimonials-card-text"><?php echo $content ?></div>
						<div class="testimonials-card-person">
							<span class="testimonials-card-name"><?php the_field('name'); ?></span>
							<span class"testimonials-card-occupation"><?php the_field('occupation'); ?></span>
						</div><!-- .testimonials-card-person -->
					</div><!-- .testimonials-card -->

					<div class="testimonials-thumbnail">
						<?php the_post_thumbnail(); ?>
					</div>
				</div>

				<?php endwhile; ?>
				<!-- end of the loop -->
			</div><!-- .testimonials-wrap -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'No testimonials yet', 'twentyseventeenchild' ); ?></p>
			<?php endif; ?>


		</div><!-- .wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
