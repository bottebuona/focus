<?php

// show feature in front page features Section
function focus_feature_card ($num) {
  global $childfeaturenum;
  $childfeaturenum = $num;
  get_template_part( 'template-parts/page/content', 'feature-card' );
}


// Show testimonials section on front page
function child_front_page_testimonials() {

  if ( get_theme_mod( 'panel_testimonials') ) {
    get_template_part( 'template-parts/page/content', 'front-page-panel-testimonials');
  }
  elseif ( is_customize_preview() ) {
		// The output placeholder anchor.
    echo '<article class="panel-placeholder panel twentyseventeen-panel twentyseventeenchild-paneltestimonials" id="paneltestimonials"><span class="twentyseventeen-panel-title">Front Page Testimonials Section Placeholder</span></article>';
	}

}

// Front page additional child theme section
function child_front_page_section() {

  $id = 'panel_video';

	global $post; // Modify the global post object before setting up post data.
	if ( get_theme_mod( $id ) ) {
		global $post;
		$post = get_post( get_theme_mod( $id ) );
		setup_postdata( $post );
		set_query_var( 'panel', $id );

		get_template_part( 'template-parts/page/content', 'front-page-panel-video'  );

		wp_reset_postdata();
	} elseif ( is_customize_preview() ) {
		// The output placeholder anchor.
    echo '<article class="panel-placeholder panel twentyseventeen-panel twentyseventeen-panel' . $id . '" id="panel' . $id . '"><span class="twentyseventeen-panel-title">' . sprintf( __( 'Front Page Section %1$s Placeholder', 'twentyseventeenchild' ), $id ) . '</span></article>';
	}

}
?>
