var browserSync   = require('browser-sync').create();
var gulp          = require('gulp');
var devUrl        = "http://focus.dev";



// Watch everything
gulp.task('watch', function() {
	browserSync.init({
		open: 'external',
		proxy: devUrl
	});
	gulp.watch('**/*').on('change', browserSync.reload);
});
