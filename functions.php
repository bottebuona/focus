<?php

// Enqueue parent theme style
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );
function child_theme_enqueue_styles() {
	// Dequeue parent fonts
 	wp_dequeue_style( 'twentyseventeen-fonts' );

	// Enqueue Google Fonts
	wp_enqueue_style( 'child-fonts', child_fonts_url() );

	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

  wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array( 'jquery') );
}


// Load child theme text domain
function child_theme_setup() {
    load_child_theme_textdomain( 'twentyseventeenchild', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'child_theme_setup' );


// Add SVG definitions to the footer.
function child_include_svg_icons() {
	// Define SVG sprite file.
	$svg_icons = get_theme_file_path( '/assets/images/icons.svg' );

	// If it exists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}
add_action( 'wp_footer', 'child_include_svg_icons', 9998 );


// Register custom fonts.
function child_fonts_url() {
	$fonts_url = '';
	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Raleway, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$raleway = _x( 'on', 'Raleway font: on or off', 'twentyseventeenchild' );
	$font_family = '';

	if ( 'off' !== $raleway ) {
		$font_family = 'Raleway:400,600,700';

		$query_args = array(
			'family' => urlencode( $font_family ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}


// Change default header image
add_action( 'after_setup_theme', 'child_custom_header_setup' );
function child_custom_header_setup() {

	$custom_header_args = array(
    'default-image' => get_theme_file_uri( '/assets/images/mountains.jpg' ),
	);
	add_theme_support('custom-header', $custom_header_args);

	register_default_headers( array(
		'mountains' => array(
			'url'           => '%2$s/assets/images/mountains.jpg',
			'thumbnail_url' => '%2$s/assets/images/mountains.jpg',
			'description'   => __( 'Child Default Header Image', 'twentyseventeenchild' ),
		),
	) );
}


// Create post type for testimonials
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'child-testimonials',
    array(
      'labels' => array(
        'name' => __( 'Testimonials', 'twentyseventeenchild' ),
        'singular_name' => __( 'Testimonial', 'twentyseventeenchild' ),
				'all_items' => __('All Testimonials', 'twentyseventeenchild'),
      	'add_new_item' => __('Add New Testimonial', 'twentyseventeenchild'),
      	'edit_item' => __('Edit Testimonial', 'twentyseventeenchild'),
      	'view_item' => __('View Testimonial', 'twentyseventeenchild')
      	),
      'public' => true,
//			'exclude_from_search' => true,
    	'menu_icon' => 'dashicons-format-status',
			'supports' => array('title', 'editor', 'thumbnail'),
    )
  );
}


// Child Theme Customizer options.
add_action( 'customize_register', 'child_customize_register' );
function child_customize_register( $wp_customize ) {

	// Add a customizer section for child theme options
	$wp_customize->add_section( 'child_theme_options', array(
		'title'    => __( 'Child Theme Options', 'twentyseventeenchild' ),
		'priority' => 140, // after Parent theme options
	) );


	// Add setting and control for optional front page section (Video)
	$wp_customize->add_setting( 'panel_video' , array(
		'default'           => false,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'panel_video' , array(
		'label'          => 'Front Page Video Section Content',
		'description'    => 'Select a page that contain text and video. Use a page based on the "Video Panel Template"',
		'section'        => 'child_theme_options',
		'type'           => 'dropdown-pages',
		'allow_addition' => true,
		'active_callback' => 'twentyseventeen_is_static_front_page',
	) );

	$wp_customize->selective_refresh->add_partial( 'panel_video' , array(
		'selector'            => '#panelvideo',
		'render_callback'     => 'child_front_page_section',
		'container_inclusive' => true,
	) );

	// Add setting and control for optional front page section (Testimonials)
	$wp_customize->add_setting( 'panel_testimonials' , array(
		'default'           => false,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage'
	) );

	$wp_customize->add_control( 'panel_testimonials' , array(
			'label'          	=> 'Show Testimonials section on Front Page',
		   'description'    => ( 1 !== $i ? '' : __( '(Add testimonials in the admin menu)', 'twentyseventeenchild' ) ),
			'section'        	=> 'child_theme_options',
			'type'						=> 'checkbox',
			'active_callback' => 'twentyseventeen_is_static_front_page'
		) );

		$wp_customize->selective_refresh->add_partial( 'panel_testimonials' , array(
				'selector'            => '#paneltestimoniasl',
				'render_callback'     => 'child_front_page_testimonials',
				'container_inclusive' => true,
			) );
}


// new menu for child theme
function register_child_menu() {
  register_nav_menu('contact',__( 'Contact Menu', 'twentyseventeenchild' ));
}
add_action( 'init', 'register_child_menu' );


// Additional child theme template tags
require get_theme_file_path( '/inc/child-template-tags.php' );

?>
