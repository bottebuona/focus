
(function( $ ) {

  // Fire on document ready.
  $( document ).ready( function() {

    $('.testimonials-item:first-child').addClass('testimonials-item-active');
    $('.testimonials-thumbnail').click( function(){
      console.log("testimonialsChange");
      var $thumbnail = $(this);
      var $active = $('.testimonials-item-active');
      if( $active !== $thumbnail.parent() ) {
        $active.removeClass('testimonials-item-active');
        $thumbnail.parent().addClass('testimonials-item-active');
      }
    } );

    // change arrow down image - point to different svg
    var $svgTarget = $('svg.icon-arrow-right use');
    $svgTarget.attr('xlink:href', '#icon-arrow-down');
    $svgTarget.attr('href', '#icon-arrow-down');

    var $body = $( 'body' ),
    $menuScrollDown = $body.find( '.menu-scroll-down' );

    // wrap arrow down elemente in header to easy center it
    $menuScrollDown.wrap('<div class="arrow-wrapper">', '</div>');

    // remove focus from arrow when clicked
    $menuScrollDown.click( function() {
      $(this).blur();
    });

    // Find all iframes
    var $iframes = $( "iframe" );

    // Find and save the aspect ratio for all iframes
    $iframes.each(function () {
      $( this ).data( "ratio", this.height / this.width )
      // Remove the hardcoded width &#x26; height attributes
      .removeAttr( "width" )
      .removeAttr( "height" );
    });

    // Resize the iframes when the window is resized
    $( window ).resize( function () {
      $iframes.each( function() {
        // Get the parent container&#x27;s width
        var width = $( this ).parent().width();
        $( this ).width( width )
        .height( width * $( this ).data( "ratio" ) );
      });
      // Resize to fix all iframes on page load.
    }).resize();


  });

})( jQuery );
